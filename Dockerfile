FROM node:16-alpine AS builder
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./


RUN npm ci --silent
COPY . ./
RUN npm run build

FROM nginx:1.19.6-alpine
COPY --from=builder /app/dist /var/www/html

COPY ./deploy_config/nginx.conf /etc/nginx/nginx.conf
COPY ./deploy_config/default.conf /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /var/www/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
