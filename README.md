# dashboard

## Project description
In dit project was het de bedoeling een dashboard te maken voor het bedrijf DrTrottoir. Dit is een bedrijf waarbij studenten de nodige afval buiten zetten van meerdere grote gebouwen en deze indien nodig terug binnen zetten. Hiervoor was er een heel systeem nodig waarbij routes en gebouwen kunnen worden aangemaakt zodat studenten hierop kunnen toegewezen worden. Een methode om de planning van de afvalophaling per gebouw eenvoudig in te geven was hierbij ook nodig. Voor de uitwerking hiervan hebben wij gekozen voor Vue als frontend-framework en ASP.Net core als bakcend.
Een bijkomend project was een app voor deze studenten om hun toegewezen routes te kunnen zien, met een lijst van gebouwen met hun instructies en velden om foto's te maken en direct up te loaden als bewijs. Dit werd gedaan in Flutter. (Hieraan heb ik niet gewerkt, dus heb ik deze niet toegevoegd aan dit portfolio)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Execute tests
```
npm run test
```
## URLs
### Development

| App           | URL |
|---------------| --- |
| Website       | https://dt-dev.tygo.io/ |
| Api-endpoint  | https://dt-dev.tygo.io/api/ |
| Api-swagger   | https://dt-dev.tygo.io/swagger/index.html |
| App-webrender | https://dt-dev.tygo.io/app |

### Production

| App           | URL |
|---------------| --- |
| Website       | https://dt.tygo.io/ |
| Api-endpoint  | https://dt.tygo.io/api/ |
| Api-swagger   | https://dt.tygo.io/swagger/index.html |

