import { createRouter, createWebHistory } from "vue-router";
import HomeView from "@/components/HomeView.vue";
import RoutesView from "@/components/RoutesView.vue";
import RoutesDetail from "@/components/RoutesDetail.vue";
import AccountsView from "@/components/AccountsView.vue";
import AccountsDetail from "@/components/AccountsDetail.vue";
import AddAccountsView from "@/components/AddAccountsView.vue";
import BuildingsView from "@/components/BuildingsView.vue";
import AddBuildingsView from "@/components/AddBuildingsView.vue";
import AddRoutesView from "@/components/AddRoutesView.vue";
import BuildingsDetail from "@/components/BuildingsDetail.vue";
import BuildingsShedule from "@/components/BuildingsShedule.vue";
import store from "@/store/auth.js";
import LoginView from "@/components/LoginView.vue";
import LogoutView from "@/components/LogoutView.vue";
import UpdateBuilding from "@/components/UpdateBuilding.vue";
import UpdateRoute from "@/components/UpdateRoute.vue";

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: [
    {
      path: "",
      component: HomeView,
      name: "home",
      props: true,
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/routes",
      component: RoutesView,
      name: "routes",
      props: true,
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/routes/add",
      component: AddRoutesView,
      name: "addRoutes",
      props: true,
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/routes/:id",
      component: RoutesDetail,
      name: "RoutesDetail",
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/routes/edit/:id",
      component: UpdateRoute,
      name: "editRoutes",
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/accounts",
      component: AccountsView,
      name: "accounts",
      props: true,
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/accounts/add",
      component: AddAccountsView,
      name: "addAccounts",
      props: true,
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/accounts/:id",
      component: AccountsDetail,
      name: "AccountsDetail",
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/buildings",
      component: BuildingsView,
      name: "buildings",
      props: true,
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/buildings/add",
      component: AddBuildingsView,
      name: "addBuildings",
      props: true,
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/buildings/:id",
      component: BuildingsDetail,
      name: "BuildingsDetail",
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/buildings/edit/:id",
      component: UpdateBuilding,
      name: "editBuildings",
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/buildings/shedule",
      component: BuildingsShedule,
      name: "BuildingsShedule",
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/login",
      component: LoginView,
      name: "login",
      props: true,
      beforeEnter: (to, from, next) => {
        if (!store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/" });
      },
    },
    {
      path: "/logout",
      component: LogoutView,
      name: "logout",
      props: true,
      beforeEnter: (to, from, next) => {
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    { path: "/:catchAll(.*)", redirect: { path: "/" } },
  ],
});

router.beforeEach(async (to, from, next) => {
  store.commit("general/setLoading", true);
  await store.dispatch("auth/tryAutoLogin");
  store.commit("general/setLoading", false);
  next();
});
export default router;
