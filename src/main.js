import axios from "axios";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index.js";
import store from "./store/auth.js";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";

import openlayersMap from "vue3-openlayers";
import "vue3-openlayers/dist/vue3-openlayers.css";

axios.defaults.withCredentials = true;
axios.defaults.baseURL = process.env.BASE_URL;
//axios.defaults.headers.common["Accept"] = "application/json";

const app = createApp(App);
app.use(store);
app.use(router);
app.use(openlayersMap);

app.mount("#app");
