import axios from "axios";
import { createStore } from "vuex";
import router from "../router";

const store = createStore({
  modules: {
    auth: {
      namespaced: true,
      state: {
        loggedIn: false,
        triedAutoLogin: false,
        user: {},
      },
      getters: {
        isLoggedIn(state) {
          return state.loggedIn;
        },
        getUser(state) {
          return state.user;
        },
      },
      actions: {
        async sendCode(_, input) {
          let data = await axios
            .post("/api/auth/", input, {
              withCredentials: true,
            })
            .then(async (response) => {
              return response;
            });
          return data;
        },
        async login({ dispatch, commit }, input) {
          let data = await axios
            .post("/api/auth/complete/", input, {
              withCredentials: true,
            })
            .then(async (response) => {
              if (response.status === 200) {
                await dispatch("fetchUser");
                commit("setTriedAutoLogin", true);
              }
              return response;
            });
          return data;
        },
        async logout({ commit }) {
          commit("general/setLoading", true, { root: true });
          commit("setUser", {});
          commit("setLoggedIn", false);
          await axios
            .get("/api/auth/logout", {
              withCredentials: true,
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
              },
            })
            .then((response) => {
              commit("setTriedAutoLogin", true);
              commit("setUser", response.data);
              commit("setLoggedIn", true);
            });

          commit("setTriedAutoLogin", false); // Clear the auto login flag as well
          commit("general/setLoading", false, { root: true });
          router.push({ name: "login" });
        },
        async tryAutoLogin({ commit, dispatch, state }) {
          if (!state.triedAutoLogin) {
            try {
              console.log("trying autoLogin");
              commit("setTriedAutoLogin", true);
              await dispatch("fetchUser");
            } catch (error) {
              console.log(error);
              commit("setLoggedIn", false);
              commit("setUser", {});
              window.localStorage.removeItem("token");
              window.localStorage.removeItem("user");
              window.localStorage.removeItem("refresh_token");
              console.log("autoLogin failed");
            }
          }
        },
        fetchUser({ commit }) {
          return axios
            .get("/api/user", {
              withCredentials: true,
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
              },
            })
            .then((response) => {
              commit("setUser", response.data);
              commit("setLoggedIn", true);
            });
        },
      },
      mutations: {
        setLoggedIn(state, value) {
          state.loggedIn = value;
        },
        setUser(state, value) {
          state.user = value;
        },
        setTriedAutoLogin(state, value) {
          state.triedAutoLogin = value;
        },
      },
    },
    general: {
      namespaced: true,

      state: {
        loading: false,
      },

      getters: {
        isLoading(state) {
          return state.loading;
        },
      },

      mutations: {
        setLoading(state, value) {
          state.loading = value;
        },
      },
    },
  },
});

export default store;
