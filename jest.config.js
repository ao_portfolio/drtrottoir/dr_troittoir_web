module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  collectCoverage: true,
  collectCoverageFrom: [
    "**/*.{js,vue}",
    "!**/*.config.js",
    "!**/{node_modules,coverage,dist}/**",
  ],
  coverageReporters: ["clover", "json", "lcov", "text", "cobertura"],
  verbose: true,
  injectGlobals: true,
};
